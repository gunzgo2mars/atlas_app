import { Component, OnInit } from '@angular/core';
import { NavController , AlertController } from '@ionic/angular'
import { FetchdataService } from '../fetchdata.service'


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})


export class SignupPage implements OnInit {

  astronautId : string
  astronautPassword : string

  constructor(private navCtrl : NavController , private alertCtrl : AlertController, private fetchData : FetchdataService) { }

  ngOnInit() {}

  alertModal = async (message:string) => {

    const alert = await this.alertCtrl.create({

      header : 'Result',
      message : message,
      buttons : ['OK']

    })

    await alert.present()

  }
  
  serviceSignUp = async () => {

    const post = await this.fetchData.sendDataUser(this.astronautId , this.astronautPassword , 'signup')
    post.subscribe(data => {

      const response = data.json()

      if(response.error == false) {
        
        this.alertModal(response.message)

      } else {

        this.alertModal(response.message)

      }

    })
  }
  
  goBack = () => this.navCtrl.goBack()



}
