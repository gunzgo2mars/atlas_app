import { Component } from '@angular/core';
import { NavController , AlertController } from '@ionic/angular'
import { FetchdataService } from '../fetchdata.service'
import { ParamsService } from '../params.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  astronautId : string
  astronautPassword : string

  constructor(private paramsService : ParamsService , private navCtrl : NavController , private alertCtrl : AlertController , private fetchData : FetchdataService) {}

  alertModal = async (message:string) => {

    const alert = await this.alertCtrl.create({
      header : 'Result',
      message : message,
      buttons : ['OK']
    })

    await alert.present()

  }

  sendData = async () => {

    const post = await this.fetchData.sendDataUser(this.astronautId , this.astronautPassword , 'login')
    await post.subscribe(data => {

      const response = data.json()

      if(response.error == false) {

        this.paramsService.setKey(this.astronautId)
        this.navCtrl.navigateForward('article')

      }
      else { this.alertModal(response.message) } 

    })

  }
  
  signUpNavigate = () => this.navCtrl.navigateForward('signup')



}
