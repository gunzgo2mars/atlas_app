import { Component, OnInit } from '@angular/core';
import { NavController , AlertController } from '@ionic/angular'
import { ParamsService } from '../params.service'
import { FetchdataService } from '../fetchdata.service'

@Component({
  selector: 'app-addfriend',
  templateUrl: './addfriend.page.html',
  styleUrls: ['./addfriend.page.scss'],
})
export class AddfriendPage implements OnInit {

  friendId : string

  constructor(private navCtrl : NavController , private alertCtrl : AlertController , private paramsService : ParamsService , private fetchData : FetchdataService) {

    console.log(this.paramsService.getKey())

  }

  ngOnInit() {
  }

  alertModal = async (message:string) => {

    const alert = await this.alertCtrl.create({

      header : 'Result',
      message : message,
      buttons : ['OK']

    })

    await alert.present()

  }

  sendData = async () => {

    const post = await this.fetchData.sendDataNewfriend(this.paramsService.getKey() , this.friendId)
    post.subscribe(data => {

      const response = data.json()
      if(response.error == false) { this.alertModal(response.message) }
      else { this.alertModal(response.message) }
      

    })

  }

  goBack = () => this.navCtrl.goBack()

}
