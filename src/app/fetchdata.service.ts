import { Injectable } from '@angular/core';
import { Http } from '@angular/http'
import 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class FetchdataService {

  constructor(private http : Http) { }

  sendDataUser = (astronuatId:string , astronuatPassword:string , apiType:string) => {

    if(apiType == 'login') {

      return this.http.post('https://gunz-atlas-api.herokuapp.com/api/v1/data/userRequestLogin' , {

        astronautId : astronuatId,
        astronautPassword : astronuatPassword

      })

    }  

    if(apiType == 'signup') {

      return this.http.post('https://gunz-atlas-api.herokuapp.com/api/v1/data/createNewUser' , {

        astronautId : astronuatId,
        astronautPassword : astronuatPassword

      })

    }

  }

  sendDataNewfriend = (astronautId:string , friendId:string) => {

    return this.http.post('https://gunz-atlas-api.herokuapp.com/api/v1/data/requestAddNewFriend' , {

      astronautId : astronautId,
      friendId : friendId

    })

  }

  fetchFriendById = (astronautId:string) => {

    return this.http.post('https://gunz-atlas-api.herokuapp.com/api/v1/data/getFriendById' , {

      astronautId : astronautId

    })

  }

  deleteFriendByObjectId = (ObjectId:string) => {

    return this.http.post('https://gunz-atlas-api.herokuapp.com/api/v1/data/deleteFriendById' , {

      _id : ObjectId

    })

  }


}
