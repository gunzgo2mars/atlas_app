import { Component, OnInit } from '@angular/core'
import { NavController } from '@ionic/angular'
import { ParamsService } from '../params.service'
import { FetchdataService } from '../fetchdata.service' 
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

  key : string
  friend : any = []

  constructor(private fetchData : FetchdataService, private paramsService : ParamsService , private navCtrl : NavController) { 

    this.key = this.paramsService.getKey()
    console.log(this.paramsService.getKey())
    this.getDataById()

  }

  ngOnInit() {
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.getDataById()
    setTimeout(() => {
      event.target.complete()
    }, 2000);
  }

  getDataById = async () => {


    const post = await this.fetchData.fetchFriendById(this.paramsService.getKey())
    await post.subscribe(data => {

      this.friend = data.json()

    })

  }

  deleteData =  async (ObjectId:string , event) => {

    const post = await this.fetchData.deleteFriendByObjectId(ObjectId)
    await post.subscribe(data => {

      const response = data.json()

      if(response.error == false) { this.doRefresh(event) }

    })

  }

  navigateAdd = async () => {

    await this.paramsService.setKey(this.key)
    await this.navCtrl.navigateForward('addfriend')
    
  }

  goBack = () => this.navCtrl.goBack()

}
