import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular'

@Injectable({
  providedIn: 'root'
})
export class ParamsService {

  key : string

  constructor(public nav : NavController) { }
  
  setKey = (key : string) => { this.key = key }
  getKey = () => { return this.key }


}
